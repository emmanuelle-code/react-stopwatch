import React, {useState, useEffect} from 'react';

let m = 0;
let s = 0;
let ms = 0;

export default function useTimewatch() {
    const [minutes, setminutes] = useState(0);
    const [seconds, setseconds] = useState(0);
    const [milliseconds, setmilliseconds] = useState(0);
    const [isRunning, setisRunning] = useState(false);
    const [interval, setinterval] = useState(null);

    const pause = () => {
        if (interval === null) return;
        setisRunning(false);
        clearInterval(interval);
    }

    const run = () => {
        setisRunning(true);
    }

    const clear = () => {
        if (!isRunning) {
            pause();
            m = 0;
            s = 0;
            ms = 0;
            setmilliseconds(0);
            setseconds(0);
            setminutes(0);
        }
    }

    useEffect(() => {
        let upgradeStopwatch = () => {
            if (!isRunning || minutes === 60) return;
            if (ms < 1000) ms += 10;
            else {
                ms = 0;
                if (s < 60) s ++;
                else {
                    s = 0;
                    m ++;
                    setminutes(m);
                }
                setseconds(s);
            }
            setmilliseconds(parseInt(ms / 10));
        }
        let i = setInterval(() => upgradeStopwatch(), 10);
        setinterval(i);
    }, [isRunning])

    return [milliseconds, seconds, minutes, pause, run, clear];
}
