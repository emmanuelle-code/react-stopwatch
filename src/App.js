import React from 'react';
import useTimewatch from './hooks/useTimewatch';
import {formatDigit} from './helper';

function App() {
  const [milliseconds, seconds, minutes, pause, run, clear] = useTimewatch();

  return (
    <div className='screen'>
      <div  className='timewatch'>
        <div>{formatDigit(minutes)}</div>: <div>{formatDigit(seconds)}</div>: <div>{formatDigit(milliseconds)}</div>
      </div>
      <div>
        <button className='stop' onClick={pause}>Stop</button>
        <button className='run' onClick={run}>Run</button>
        <button className='clear' onClick={clear}>Clear</button>
      </div>
    </div>
    
  );
}

export default App;
